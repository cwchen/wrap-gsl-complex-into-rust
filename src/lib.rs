pub mod gsl {
    use std::fmt;
    use std::ops::Add;

    #[repr(C)]
    pub struct CComplex {
        array: [f64; 2],
    }

    impl Copy for CComplex {}

    impl Clone for CComplex {
        fn clone(&self) -> CComplex {
            *self
        }
    }

    #[link(name = "gsl")]
    extern {
        fn gsl_complex_rect(x: f64, y: f64) -> CComplex;
        fn gsl_complex_add(a: CComplex, b: CComplex) -> CComplex;
    }

    pub struct Complex {
        complex: CComplex,
    }

    impl Complex {
        pub fn new(x: f64, y: f64) -> Complex {
            let complex = unsafe { gsl_complex_rect(x, y) };
            Complex{ complex: complex }
        }
    }

    impl Copy for Complex {}

    impl Clone for Complex {
        fn clone(&self) -> Complex {
            *self
        }
    }

    impl Add for Complex {
        type Output = Complex;

        fn add(self, other: Complex) -> Complex {
            let complex = unsafe {
                gsl_complex_add(self.complex, other.complex)
            };
            Complex{ complex: complex }
        }
    }

    impl fmt::Display for Complex {
        fn fmt(& self, f: &mut fmt::Formatter) -> fmt::Result {
            write!(f, "({}, {})", self.real(), self.imag())
        }
    }

    impl Complex {
        pub fn real(& self) -> f64 {
            self.complex.array[0]
        }
    }

    impl Complex {
        pub fn imag(& self) -> f64 {
            self.complex.array[1]
        }
    }
}
