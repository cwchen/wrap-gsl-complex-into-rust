extern crate gslcomplex;

use gslcomplex::gsl;

fn main() {
    let c1 = gsl::Complex::new(2.0, 4.0);
    let c2 = gsl::Complex::new(1.0, 3.0);
    let c3 = c1 + c2;
    println!("{}", c1);
    println!("{}", c2);
    println!("{}", c3);
}